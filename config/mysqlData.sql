CREATE TABLE warehouse 
( id_warehouse smallint unsigned not null auto_increment,
                 name varchar(20) not null,
                 min_product int not null,
                 max_product int not null,
                 constraint pk_warehouse_id primary key (id_warehouse) ); 
                 
CREATE TABLE product 
( id_product smallint unsigned not null auto_increment,
                 name varchar(20) not null,
                 totalQty int not null,
                 remaining int not null,
                 constraint pk_product_id primary key (id_product),
                 id_warehouse smallint not null
                
                  ); 
                 
ALTER table product
ADD FOREIGN KEY fk_prod(id_warehouse)
REFERENCES warehouse(id_warehouse)
ON DELETE cascade
ON UPDATE cascade;