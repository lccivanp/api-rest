var express = require('express');
var app = express.Router();



var mysql = require('mysql');
var bd = require('../config/bd.js');
// connection configurations
const mc = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'test'
});
 
// connect to database
mc.connect();
 
// default route
app.get('/', function (req, res) {
    return res.send({ error: true, message: 'hello' })
});
 
 
// Retrieve all warehouse 
app.get('/warehouse', function (req, res) {
    mc.query('SELECT * FROM warehouse', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'warehouse.' });
    });
});


 
// Retrieve warehouse with id 
app.get('/warehouse/:id', function (req, res) {
 
    let warehouse_id = req.params.id;
 
    mc.query('SELECT * FROM warehouse where id_warehouse=?', warehouse_id, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results[0], message: 'warehouse list.' });
    });
 
});
 
// Add a new warehouse  
app.post('/warehouse', function (req, res) {
 
    let warehouse = req.body.warehouse;
 
    if (!warehouse) {
        return res.status(400).send({ error:true, message: 'Please provide warehouse' , body: req.body});
    }
 
    mc.query("INSERT INTO warehouse SET ? ", { name : warehouse.name, min_product: warehouse.min_product, max_product: warehouse.max_product }, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'New warehouse has been created successfully.' });
    });
});
 
//  Update todo with id
app.put('/warehouse', function (req, res) {
 
    let warehouse_id = req.body.warehouse_id;
    let name = req.body.name;
    let max = req.body.max;
    let min = req.body.min;
    if (!task_id || !task) {
        return res.status(400).send({ error: task, message: 'Please provide task and task_id' });
    }
 
    mc.query("UPDATE warehouse SET name = ? , min_product = ?, max_product = ? WHERE id_warehouse = ?", [name, min, max, warehouse_id], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Task has been updated successfully.' });
    });
});
 
//  Delete todo
app.delete('/warehouse/:id', function (req, res) {
 
    let warehouse_id = req.params.id;
 
    mc.query('DELETE FROM warehouse WHERE id_warehouse = ?', [warehouse_id], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Warehouse has been updated successfully.' });
    });
 
});
 
// all other requests redirect to 404
app.all("*", function (req, res, next) {
    return res.send('page not found');
    next();
});


module.exports = app;