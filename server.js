
	
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mysql = require('mysql');
var cors = require('cors')
//app.use(cors);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
 
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
// connection configurations
const mc = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'test'
});
 
// connect to database
mc.connect();
 
// default route
app.get('/', function (req, res) {
    return res.send({ error: true, message: 'hello' })
});
 
 
// Retrieve all warehouse 
app.get('/warehouse', function (req, res) {
    mc.query('SELECT * FROM warehouse', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'warehouse.' });
    });
});


 
// Retrieve warehouse with id 
app.get('/warehouse/:id', function (req, res) {
 
    let warehouse_id = req.params.id;
 
    mc.query('SELECT * FROM warehouse where id_warehouse=?', warehouse_id, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results[0], message: 'warehouse list.' });
    });
 
});
 
// Add a new warehouse  
app.post('/warehouse', function (req, res) {
 
    let warehouse = req.body.warehouse;
 
    if (!warehouse) {
        return res.status(400).send({ error:true, message: 'Please provide warehouse' , body: req.body});
    }
 
    mc.query("INSERT INTO warehouse SET ? ", { name : warehouse.name, min_product: warehouse.min_product, max_product: warehouse.max_product }, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'New warehouse has been created successfully.' });
    });
});
 
//  Update todo with id
app.put('/warehouse', function (req, res) {
 
    let warehouse_id = req.body.warehouse_id;
    let name = req.body.name;
    let max = req.body.max;
    let min = req.body.min;
    if (!task_id || !task) {
        return res.status(400).send({ error: task, message: 'Please provide task and task_id' });
    }
 
    mc.query("UPDATE warehouse SET name = ? , min_product = ?, max_product = ? WHERE id_warehouse = ?", [name, min, max, warehouse_id], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Task has been updated successfully.' });
    });
});
 
//  Delete todo
app.delete('/warehouse/:id', function (req, res) {
 
    let warehouse_id = req.params.id;
 
    mc.query('DELETE FROM warehouse WHERE id_warehouse = ?', [warehouse_id], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Warehouse has been updated successfully.' });
    });
 
});
 
/*
PRODUCT
*/


 
// Retrieve all product 
app.get('/product', function (req, res) {
    mc.query('SELECT * FROM product', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'product.' });
    });
});


 
// Retrieve product with id 
app.get('/product/:id', function (req, res) {

    let product_id = req.params.id;
 
    mc.query('SELECT * FROM product where id_product=?', product_id, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results[0], message: 'product list.' });
    });
 
});
 
// Add a new warehouse  
app.post('/product', function (req, res) {
 
    let product = req.body.product;
 
    if (!product) {
        return res.status(400).send({ error:true, message: 'Please provide product' , body: req.body});
    }
 
    mc.query("INSERT INTO product SET ? ", 
        {   
            name : product.name,
            totalQty: product.totalQty, 
            remaining: product.remaining ,
            id_warehouse: product.id_warehouse
        }, function (error, results, fields) {
            if (error) throw error;
            return res.send({ error: false, data: results, message: 'New product has been created successfully.' });
    });
});
 
//  Update todo with id
app.put('/product', function (req, res) {
 
    let id_product = req.body.id_product;
    let totalQty = req.body.totalQty;
    let remaining = req.body.remaining;
    let id_warehouse = req.body.id_warehouse;
    if (!id_product || !name) {
        return res.status(400).send({ error: task, message: 'Please provide product and product_id' });
    }
 
    mc.query("UPDATE product SET name = ? , totalQty = ?, remaining = ? , id_warehouse= ? WHERE id_warehouse = ?", 
        [name, totalQty, remaining, id_warehouse, id_product], 
    function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Product has been updated successfully.' });
    });
});
 
//  Delete todo
app.delete('/product/:id', function (req, res) {
 
    let product_id = req.params.id;
 
    mc.query('DELETE FROM product WHERE id_product = ?', [product_id], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Product has been updated successfully.' });
    });
 
});



// all other requests redirect to 404
app.all("*", function (req, res, next) {
    return res.send('page not found');
    next();
});
 
// port must be set to 8080 because incoming http requests are routed from port 80 to port 8080
app.listen(8080, function () {
    console.log('Node app is running on port 8080');
});

 
// allows "grunt dev" to create a development server with livereload
module.exports = app;